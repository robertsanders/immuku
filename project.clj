(defproject immuku "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://immuku.heroku.com/"
  :min-lein-version "2.0"
  :plugins [[lein-immutant "0.15.1"]]
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.4.0"]])
